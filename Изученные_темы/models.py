from django.db import models


# Create your models here.
class Темы(models.Model):
    """Темы которые я изучил"""

    name = models.CharField(max_length=20)
    date_added = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        """возращает строковое представление модели"""
        return self.name


# class Записи(models.Model):
#   """Информация по изученым темам"""

#  Темы = models.ForeignKey(Темы)
# text = models.TextField()
# date_added = models.DateTimeField(auto_now_add=True)

# class Meta:
#   verbose_name_plural = "Темы"

# def word(self, text):
#   """для определения челого слова"""
#  a = text[50:]
# for n in range(len(a)):
#    if a[n] == " ":
#       return text[: 50 + n] + "..."
#  else:
#     continue

# def __str__(self):
#   """возращает строковое представление модели"""
#  if len(self.text) > 50:
#     return self.word(self.text)

# else:
#   return self.text
