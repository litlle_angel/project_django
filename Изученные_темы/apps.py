from django.apps import AppConfig


class ИзученныеТемыConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Изученные_темы'
